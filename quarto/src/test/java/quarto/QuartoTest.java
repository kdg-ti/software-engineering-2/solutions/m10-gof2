package quarto;

import org.junit.jupiter.api.Test;
import quarto.QuartoBlock;
import quarto.QuartoFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Jan de Rijke.
 */
public class QuartoTest {
	@Test
	public void createBlock(){
		QuartoBlock b1 = QuartoFactory.createBlock(
			QuartoBlock.Length.SHORT,
			QuartoBlock.Color.DARK,
			QuartoBlock.Shape.CIRCULAR,
			QuartoBlock.Volume.HOLLOW);

		QuartoBlock b2 = QuartoFactory.createBlock(
			QuartoBlock.Length.SHORT,
			QuartoBlock.Color.LIGHT,
			QuartoBlock.Shape.CIRCULAR,
			QuartoBlock.Volume.SOLID);

		QuartoBlock b3 = QuartoFactory.createBlock(
			QuartoBlock.Length.SHORT,
			QuartoBlock.Color.DARK,
			QuartoBlock.Shape.CIRCULAR,
			QuartoBlock.Volume.HOLLOW);

		QuartoBlock b4 = QuartoFactory.createBlock(
			QuartoBlock.Length.TALL,
			QuartoBlock.Color.DARK,
			QuartoBlock.Shape.SQUARE,
			QuartoBlock.Volume.HOLLOW);

		QuartoBlock b5 = QuartoFactory.createBlock(
			QuartoBlock.Length.TALL,
			QuartoBlock.Color.DARK,
			QuartoBlock.Shape.CIRCULAR,
			QuartoBlock.Volume.HOLLOW)	;
		assertEquals(5,QuartoFactory.countBlocks(),"Factory should have created 5 blocks");
		assertEquals(b1,b3);
	}
}
