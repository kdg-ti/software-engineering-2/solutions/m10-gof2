package quarto;

import java.util.HashSet;
import java.util.Set;

/**
 * Todo: Werk uit volgens de instructies.
 */

public class QuartoFactory {

	private static Set<QuartoBlock> aangemaakt = new HashSet();
	private static int teller = 0;


	private QuartoFactory() {
	}

	public static QuartoBlock createBlock(
		QuartoBlock.Length length, QuartoBlock.Color
		color,
		QuartoBlock.Shape shape, QuartoBlock.Volume volume
	) {
		teller++;
		for (QuartoBlock block : aangemaakt) {
			if (block.equals(color ,length ,  shape , volume)) {
				return block;
			}
		}
		QuartoBlock newBlock = new QuartoBlock(length, color, shape, volume);
		aangemaakt.add(newBlock);
		return newBlock;
	}

	public static int countBlocks(){
		return teller;
	}

}

