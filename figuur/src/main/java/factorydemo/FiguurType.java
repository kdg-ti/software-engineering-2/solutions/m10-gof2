package factorydemo;

/**
 * @author Jan de Rijke.
 */
public enum FiguurType {
	RECHTHOEK, RUIT, VIERKANT
}
