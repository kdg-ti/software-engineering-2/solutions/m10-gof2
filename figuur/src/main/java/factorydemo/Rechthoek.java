package factorydemo;

/**
 * @author Jan de Rijke.
 */
public class Rechthoek implements Figuur {
	double hoogte,breedte;

	public double getHoogte() {
		return hoogte;
	}

	public double getBreedte() {
		return breedte;
	}

	public Rechthoek (double zijde){
		this(zijde,zijde);
	}

	public Rechthoek(double lengte, double breedte) {
		this.hoogte = lengte;
		this.breedte = breedte;
	}

	protected static Figuur newRechthoek(double v) {
		return new Rechthoek(v);
	}

	@Override
	public double oppervlakte() {
		return hoogte *breedte;
	}

	@Override
	public String toString() {
		return "Rechthoek{" +
			"hoogte=" + hoogte +  ", breedte" +breedte+
			'}';
	}

	protected static Figuur newRechthoek(double v, double v1) {
		return new Rechthoek(v, v1);
	}
}
