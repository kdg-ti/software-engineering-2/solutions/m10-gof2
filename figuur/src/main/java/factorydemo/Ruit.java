package factorydemo;

/**
 * @author Jan de Rijke.
 */
public class Ruit implements Figuur {
	double breedte, hoogte  ;

	public Ruit(double breedte,double hoogte) {
		this.breedte = breedte;
		this.hoogte=hoogte;
	}

	protected static Figuur newRuit(double breedte,double hoogte) {
		return new Ruit(breedte,hoogte);
	}

	@Override
	public double oppervlakte() {
		return breedte*hoogte/2;
	}

	public double getBreedte() {
		return breedte;
	}

	public double getHoogte() {
		return hoogte;
	}

	@Override
	public String toString() {
		return "Ruit{" +
			"breedte=" + breedte +
			", hoogte=" + hoogte +
			'}';
	}
}
