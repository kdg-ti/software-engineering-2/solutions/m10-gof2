package factorydemo;



/**
 * @author Jan de Rijke.
 */
public class FiguurFactory {
	public static Figuur getVierkant(double v) {
		return Vierkant.newVierkant(v);
	}

	public static Figuur getRechthoek(double v) {
		return Rechthoek.newRechthoek(v);
	}

	public static Figuur getFiguur(FiguurType type, double v) {
		switch (type) {
			case RECHTHOEK:
				return getRechthoek(v);
			case VIERKANT:
				return getVierkant(v);
		}
		throw new IllegalArgumentException("Creatie van figuur "+ type+ " met 1 parameter " +
			"niet " +
			"ondersteund.");
	}

	public static Figuur getRechthoek(double v, double v1) {
		return Rechthoek.newRechthoek(v,v1);
	}

	public static Figuur getRuit(double v,double v1) {
		return Ruit.newRuit(v,v1);
	}

	public static Figuur getFiguur(FiguurType type, double v, double v1) {
		switch (type) {
			case RECHTHOEK:
				return getRechthoek(v,v1);
			case RUIT:
				return getRuit(v,v1);
		}
		throw new IllegalArgumentException("Creatie van figuur "+ type+ " met 2 parameters " +
			"niet " +
			"ondersteund.");	}
}
