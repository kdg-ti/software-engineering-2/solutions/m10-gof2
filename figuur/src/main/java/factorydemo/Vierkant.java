package factorydemo;

/**
 * @author Jan de Rijke.
 */
public class Vierkant implements Figuur {
	private double zijde;
	private Vierkant(double v) {
		zijde=v;
	}

	@Override
	public String toString() {
		return "Vierkant{" +
			"zijde=" + zijde +
			'}';
	}

	public double getZijde() {
		return zijde;
	}

	@Override
	public double oppervlakte() {
		return zijde*zijde;
	}

	protected static Figuur newVierkant(double v) {
		return new Vierkant(v);
	}
}
