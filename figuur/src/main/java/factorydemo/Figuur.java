package factorydemo;

/**
 * @author Jan de Rijke.
 */
public interface Figuur {

	double oppervlakte();
}
