package be.kdg.state;

/**
 * @author Jan de Rijke.
 */
public class Frog implements CreatureState {
	@Override
	public String greet() {
		return "Ribbet!";
	}

	@Override
	public void kiss(Creature c) {
		c.setCreatureState(new Prince());
	}
}
