package be.kdg.state;

/**
 * @author Jan de Rijke.
 */
public interface CreatureState {
	String greet();

	default void kiss(Creature c){
		// do nothing
	};
}
