package be.kdg.state;

/**
 * Het gedrag van de methode greet is afhankelijk van de toestande van de boolean isFrog.
 * Pas op deze klasse het State pattern toe (maak gebruik van een interface).
 */
public class Creature {
    private CreatureState creatureState = new Frog();

    public String greet() {
        return creatureState.greet();
    }

    public void kiss() {
        creatureState.kiss(this);
    }
    public void setCreatureState(CreatureState creatureState){
        this.creatureState=creatureState;
    }
}
